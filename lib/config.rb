require 'highline'

require_relative 'players/human'
require_relative 'players/computers/easy'
require_relative 'players/computers/medium'
require_relative 'players/computers/hard'

#
# Classe controladora da inicialização das infomações necessárias para incio
# do jogo. Básicamente pede as configurações para o usuário e cria os players
#
class Config
  def initialize
    @cli = HighLine.new
  end

  def choose_marker
    marker = nil
    @cli.choose do |menu|
      menu.prompt = 'ESCOLHA O TIPO DO MARCADOR: '
      menu.choice(:X) { marker = 'X' }
      menu.choice(:O) { marker = 'O' }
    end

    marker
  end

  def choose_player_type
    type = nil
    @cli.choose do |menu|
      menu.prompt = 'ESCOLHA O TIPO DO PLAYER: '
      menu.choice(:humano) { type = :human }
      menu.choice(:facil) { type = :easy }
      menu.choice(:medio) { type = :medium }
      menu.choice(:dificil) { type = :hard }
    end

    type
  end

end

module Players
  #
  # Classe responsável por representar um CPU na pardida
  #
  class Computer < Player
    def initialize(marker, board_view)
      super

      # Dificuldade em uma escala de 0 a 1, onde 1 é o mais dificil
      @difficulty = 0.5
    end

    def play
      rand < @difficulty ? best_move : random_position
    end

    private

    #
    # Realiza melhor jogada possível
    #
    def best_move
      return 4 if @board_view.available_position? 4
      eval_best_move || random_position
    end

    #
    # Analisa todas as posições vazias tentando encontrar a melhor jogada
    #
    # @return [Integer/nil] Retorna posição da melhor jogada se existir. Caso contrário nil
    #
    def eval_best_move
      @board_view.available_positions.each do |position|
        return position if victory_move?(@marker, position)
      end

      marker = Player.oponent_marker(@marker)
      @board_view.available_positions.each do |position|
        return position if victory_move?(marker, position)
      end

      nil
    end

    #
    # Simula jogada analizando se existe possibilidade de vitória
    #
    # @param [String] marker Marcador a ser usado
    # @param [Integer] position Posição a ser marcada
    #
    # @return [Boolean] retorna true caso seja uma posição de vitória
    #
    def victory_move?(marker, position)
      result = @board_view.simulate(marker, position)
      result == :win
    end

    #
    # @return [Integer] Retorna uma posição vazia aleatória do tabuleiro
    #
    def random_position
      @board_view.available_positions.sample
    end
  end
end

require_relative '../computer.rb'

module Players
  module Computers
    #
    # Nivel Médio de dificuldade
    #
    class Medium < Computer
      def initialize(marker, board_view)
        super

        # Definindo 50% de possibilidade de realizar a melhor jogada
        @difficulty = 0.5
      end
    end
  end
end

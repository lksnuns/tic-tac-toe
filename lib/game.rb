require_relative 'board'
require_relative 'boards/view'
require_relative 'config'

#
# Classe controladora do jogo. Ela tem o papel de inicializar o jogo,
# pegar informações de players e realizar o controle do jogo.
#
class Game

  def initialize
    @board = Board.new
    @config = Config.new

    @players = create_players
  end

  #
  # Inicializa o jogo fazendo o controle das jogadas.
  #
  def start
    puts @board.show

    until @board.finished?
      play
      puts @board.show
      next_turn
    end

    puts results
  end

  private

  def results
    return unless @board.finished?

    return '<<<<<<    VÉIAAAA    >>>>>>' if @board.state == :tie

    # Devemos passar uma rodada, pois após a finalização o turno é passado de vez.
    # Assim devemos voltar a vez passada para que current_player seja o vencedor
    next_turn
    "<<<<<<< '#{current_player.marker}' é o vencedor >>>>>>>"
  end

  #
  # Analiza se jogada é valida, marcando no tabuleiro
  #
  def play
    valid_play = false
    until valid_play
      position = current_player.play
      valid_play = @board.play(current_player.marker, position)
    end
  end

  def current_player
    @players.first
  end

  def next_turn
    @players.reverse!
  end


  def create_players
    # Configurando Player 1
    type = @config.choose_player_type
    marker = @config.choose_marker
    player_one = create_player(type, marker)

    # Configurando Player 2
    type = @config.choose_player_type
    player_two = create_player(type, Player.oponent_marker(marker))

    [player_one, player_two]
  end

  #
  # Cria players a partir das informações passadas pelo usuário
  #
  # @param [symbol] type Tipo do usuário a ser criado
  # @param [String] marker Marcador utilizado pelo player
  #
  # @return [Player] Retorna player criado
  #
  def create_player(type, marker)
    return unless %i[human easy medium hard].include? type

    board_view = Boards::View.new @board
    return Players::Human.new marker, board_view if type == :human

    # Converte type e transforma na classe desejada
    klass = Object.const_get("Players::Computers::#{type.to_s.capitalize}")
    klass.new marker, board_view
  end
end

FactoryBot.define do
  factory :board_view, class: Boards::View do

    transient do
      board { build :board }
    end

    initialize_with { Boards::View.new(board) }
  end
end

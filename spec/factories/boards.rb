FactoryBot.define do
  factory :board do

    trait :with_middle_filled do
      after(:build) do |board|
        board.play('X', 4)
      end
    end

    trait :missing_x_in_position_2_for_victory do
      after(:build) do |board|
        board.play('X', 0)
        board.play('X', 1)
      end
    end

    trait :missing_8_to_tie do
      after(:build) do |board|
        board.instance_variable_set(
          :@positions,
          ['O', 'X', 'O', 'O', 'X', 'X', 'X', 'O', nil]
        )
      end
    end
  end
end

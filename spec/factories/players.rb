FactoryBot.define do
  factory :player do

    transient do
      marker 'X'
      board_view { build :board_view }
    end

    initialize_with { Player.new(marker, board_view) }
  end
end

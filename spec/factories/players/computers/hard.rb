FactoryBot.define do
  factory :player_computer_hard, parent: :player_computer, class: Players::Computers::Hard do

    transient do
      board_view { build :board_view }
    end

    initialize_with { Players::Computers::Hard.new(marker, board_view) }
  end
end

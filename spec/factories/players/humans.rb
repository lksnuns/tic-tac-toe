FactoryBot.define do
  factory :player_human, parent: :player, class: Players::Human do

    initialize_with { Players::Human.new(marker, board_view) }
  end
end
